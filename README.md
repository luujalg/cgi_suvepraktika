## CGI Suvepraktika Koduülesanne
---

### Projekti käivitamine

1. Klooni projekt.
2. Käivita projekt Android Studio arenduskeskkonnas.
3. Käivita äpp (roheline "play" nupp või `Shift` + `F10`).
4. Vali avanenud menüüst arvuti külge kinnitatud Androidi seade või loo uus virtuaalne seade ning vajuta `OK`.
5. Katseta äppi füüsilises või virtuaalses Androidi seadmes.

### Ülesannetest

Iga ülesande peale kulus umbes 5 kuni 6 tundi.