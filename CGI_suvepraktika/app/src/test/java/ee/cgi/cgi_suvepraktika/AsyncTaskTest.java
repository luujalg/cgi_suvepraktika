package ee.cgi.cgi_suvepraktika;

import android.os.AsyncTask;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ee.cgi.cgi_suvepraktika.async.CategoryListTask;
import ee.cgi.cgi_suvepraktika.async.MovieListTask;
import ee.cgi.cgi_suvepraktika.model.MovieList;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class AsyncTaskTest {
    private static MovieListTask movieListTask;
    private static CategoryListTask categoryListTask;

    @BeforeClass
    public static void createActivity() {
        movieListTask = new MovieListTask();
        categoryListTask = new CategoryListTask();
    }

    @Test
    public void taskReturnsMovieList() {
        assertTrue(movieListTask.doInBackground() != null);
    }

    @Test
    public void taskReturnsNonEmptyMoviesList() {
        assertTrue(movieListTask.doInBackground().getMovies().size() == 10);
    }

    @Test
    public void taskReturnsCategoryList() {
        assertTrue(categoryListTask.doInBackground() != null);
    }
}