package ee.cgi.cgi_suvepraktika.model;

import java.util.HashSet;
import java.util.Set;

public class CategoryList {
    private Set<Category> categories;

    public CategoryList() {
        categories = new HashSet<>();
        categories.add(new Category("Comedy"));
        categories.add(new Category("Romance"));
        categories.add(new Category("Action"));
        categories.add(new Category("Thriller"));
        categories.add(new Category("Science Fiction"));
        categories.add(new Category("Fantasy"));
        categories.add(new Category("Crime"));
        categories.add(new Category("Drama"));
    }

    public String getCategoryById(int id) {
        String categoryName = "";

        for (Category category : categories) {
            if (category.getId() == id) {
                categoryName = category.getName();
                break;
            }
        }

        return categoryName;
    }
}
