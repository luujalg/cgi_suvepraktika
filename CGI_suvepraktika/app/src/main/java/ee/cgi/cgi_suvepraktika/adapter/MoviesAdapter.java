package ee.cgi.cgi_suvepraktika.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ee.cgi.cgi_suvepraktika.R;
import ee.cgi.cgi_suvepraktika.model.CategoryList;
import ee.cgi.cgi_suvepraktika.model.Movie;
import ee.cgi.cgi_suvepraktika.model.MovieList;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder>{
    private OnItemClickListener listener;
    private MovieList movieList;

    public interface OnItemClickListener {
        void onItemClick(View itemView);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView titleTextView;
        TextView ratingTextView;
        TextView categoryTextView;
        TextView yearTextView;

        ViewHolder(final View itemView) {
            super(itemView);

            titleTextView = itemView.findViewById(R.id.movie_title);
            ratingTextView = itemView.findViewById(R.id.movie_rating);
            categoryTextView = itemView.findViewById(R.id.movie_category);
            yearTextView = itemView.findViewById(R.id.movie_year);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    listener.onItemClick(itemView);
                }
            }
        }
    }

    private MovieList movies;
    private CategoryList categories;

    public MoviesAdapter(OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public MoviesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View movieView = layoutInflater.inflate(R.layout.item_movie, parent, false);

        return new ViewHolder(movieView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Movie movie = movies.getMovieById(position);

        final TextView titleTextView = holder.titleTextView;
        titleTextView.setText(movie.getTitle());
        titleTextView.setTag(movie.getDescription());

        final TextView ratingTextView = holder.ratingTextView;
        ratingTextView.setText("Rating: " + String.valueOf(movie.getRating()) + "/10");

        final TextView categoryTextView = holder.categoryTextView;
        categoryTextView.setText(categories.getCategoryById(movie.getCategoryId()));

        final TextView yearTextView = holder.yearTextView;
        yearTextView.setText(String.valueOf(movie.getYear()));
    }

    @Override
    public int getItemCount() {
        return movies.getMovies().size();
    }

    public void setMovies(MovieList movies) {
        this.movies = movies;
    }

    public void setCategories(CategoryList categories) {
        this.categories = categories;
    }
}
