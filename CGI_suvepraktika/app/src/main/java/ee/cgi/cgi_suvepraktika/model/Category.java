package ee.cgi.cgi_suvepraktika.model;

public class Category {
    private static int idCounter = 0;
    private int id;
    private String name;

    public Category(String name) {
        this.id = idCounter++;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
