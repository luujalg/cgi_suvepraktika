package ee.cgi.cgi_suvepraktika;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;


public class MovieDetailsActivity extends AppCompatActivity {
    private static final String MOVIE_TITLE = "movie_title";
    private static final String MOVIE_YEAR = "movie_year";
    private static final String MOVIE_RATING = "movie_rating";
    private static final String MOVIE_DESCRIPTION = "movie_description";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        displayMoveDetails();
    }

    public void displayMoveDetails() {
        TextView movieTitle = findViewById(R.id.movie_title_detail);
        TextView movieYear = findViewById(R.id.movie_year_detail);
        TextView movieRating = findViewById(R.id.movie_rating_detail);
        TextView movieDescription = findViewById(R.id.movie_description_detail);

        movieTitle.setText(getIntent().getStringExtra(MOVIE_TITLE));
        movieYear.setText(getIntent().getStringExtra(MOVIE_YEAR));
        movieRating.setText(getIntent().getStringExtra(MOVIE_RATING));
        movieDescription.setText(getIntent().getStringExtra(MOVIE_DESCRIPTION));
    }
}
