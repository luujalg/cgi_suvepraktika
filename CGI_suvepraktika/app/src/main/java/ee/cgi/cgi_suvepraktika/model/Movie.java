package ee.cgi.cgi_suvepraktika.model;

public class Movie {
    private static int idCounter = 0;
    private int id;
    private String title;
    private String description;
    private int year;
    private int rating;
    private int categoryId;

    public Movie(String title, String description, int year, int rating, int categoryId) {
        this.id = idCounter++;
        this.title = title;
        this.description = description;
        this.year = year;
        this.rating = rating;
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getRating() {
        return rating;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public int getYear() {
        return year;
    }

    public String getDescription() {
        return description;
    }
}
