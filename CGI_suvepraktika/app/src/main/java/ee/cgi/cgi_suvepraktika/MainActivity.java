package ee.cgi.cgi_suvepraktika;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ee.cgi.cgi_suvepraktika.adapter.MoviesAdapter;
import ee.cgi.cgi_suvepraktika.model.CategoryList;
import ee.cgi.cgi_suvepraktika.model.MovieList;

public class MainActivity extends AppCompatActivity implements MoviesAdapter.OnItemClickListener {
    MoviesAdapter adapter;

    private static final String MOVIE_TITLE = "movie_title";
    private static final String MOVIE_YEAR = "movie_year";
    private static final String MOVIE_RATING = "movie_rating";
    private static final String MOVIE_DESCRIPTION = "movie_description";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView rvMovies = findViewById(R.id.rvMovies);

        adapter = new MoviesAdapter(this);

        new FetchMoviesTask().execute();
        new FetchCategoriesTask().execute();

        rvMovies.setAdapter(adapter);
        rvMovies.setLayoutManager(new LinearLayoutManager(this));

        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        rvMovies.addItemDecoration(itemDecoration);
    }

    public void showMovieDetails(View view) {
        Intent detailsIntent = new Intent(this, MovieDetailsActivity.class);

        TextView movieTitle = view.findViewById(R.id.movie_title);
        String movieTitleString = movieTitle.getText().toString();
        detailsIntent.putExtra(MOVIE_TITLE, movieTitleString);

        TextView movieYear = view.findViewById(R.id.movie_year);
        String movieYearString = movieYear.getText().toString();
        detailsIntent.putExtra(MOVIE_YEAR, movieYearString);

        TextView movieRating = view.findViewById(R.id.movie_rating);
        String movieRatingString = movieRating.getText().toString();
        detailsIntent.putExtra(MOVIE_RATING, movieRatingString);

        String movieDescription = (String) movieTitle.getTag();
        detailsIntent.putExtra(MOVIE_DESCRIPTION, movieDescription);

        startActivity(detailsIntent);
    }

    @Override
    public void onItemClick(View itemView) {
        showMovieDetails(itemView);
    }

    public class FetchMoviesTask extends AsyncTask<Void, Void, MovieList> {

        @Override
        protected MovieList doInBackground(Void... voids) {
            return new MovieList();
        }

        @Override
        protected void onPostExecute(MovieList movieList) {
            adapter.setMovies(movieList);
        }
    }

    public class FetchCategoriesTask extends AsyncTask<Void, Void, CategoryList> {

        @Override
        protected CategoryList doInBackground(Void... voids) {
            return new CategoryList();
        }

        @Override
        protected void onPostExecute(CategoryList categoryList) {
            adapter.setCategories(categoryList);
        }
    }
}
