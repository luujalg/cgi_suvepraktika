package ee.cgi.cgi_suvepraktika.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MovieList {
    private List<Movie> movies;

    public MovieList() {
        movies = new ArrayList<>();
        movies.add(new Movie("Ice Age 3: Dawn of the Dinosaurs",
                "When Sid's attempt to adopt three dinosaur eggs gets him abducted by " +
                "their real mother to an underground lost world, his friends attempt to " +
                "rescue him.",
                2009, 7, 0));
        movies.add(new Movie("Doctor Strange",
                "While on a journey of physical and spiritual healing, a brilliant " +
                "neurosurgeon is drawn into the world of the mystic arts.",
                2016, 8, 2));
        movies.add(new Movie("The Bourne Identity",
                "A man is picked up by a " +
                "fishing boat, bullet-riddled and without memory, then races to elude assassins " +
                "and recover from amnesia.",
                2002, 8, 3));
        movies.add(new Movie("Nightcrawler",
                "When Lou Bloom, a driven man " +
                "desperate for work, muscles into the world of L.A. crime journalism, he blurs " +
                "the line between observer and participant to become the star of his own story. " +
                "Aiding him in his effort is Nina, a TV-news veteran.",
                2014, 8, 6));
        movies.add(new Movie("The Machinist",
                "An industrial worker who hasn't " +
                "slept in a year begins to doubt his own sanity.",
                2004, 8, 7));
        movies.add(new Movie("Life of Pi",
                "The story of an Indian boy named Pi, " +
                "a zookeeper's son who finds himself in the company of a hyena, zebra, orangutan, " +
                "and a Bengal tiger after a shipwreck sets them adrift in the Pacific Ocean.",
                2012, 8, 7));
        movies.add(new Movie("The King's Speech",
                "The story of King George VI of " +
                "Britain, his impromptu ascension to the throne and the speech therapist who " +
                "helped the unsure monarch become worthy of it.",
                2010, 8, 7));
        movies.add(new Movie("Logan",
                "In the near future, a weary Logan cares for " +
                "an ailing Professor X somewhere on the Mexican border. However, Logan's attempts " +
                "to hide from the world and his legacy are upended when a young mutant arrives, " +
                "pursued by dark forces.",
                2017, 8,
                2));
        movies.add(new Movie("The Hobbit: The Desolation of Smaug",
                "The dwarves, " +
                "along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim " +
                "Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a " +
                "mysterious and magical ring.",
                2013, 8, 5));
        movies.add(new Movie("Interstellar",
                "A team of explorers travel through " +
                "a wormhole in an attempt to find a potentially habitable planet that will " +
                "sustain humanity.",
                2014, 9, 4));
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public Movie getMovieById(int id) {
        for (Movie movie : movies) {
            if (movie.getId() == id) {
                return movie;
            }
        }

        return null;
    }
}
